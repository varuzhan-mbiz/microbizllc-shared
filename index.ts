import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule }        from "@angular/common";
import { FormsModule }         from "@angular/forms";
import { RouterModule }         from "@angular/router";


import { SplitUnderlinesPipe } from "./src/pipes/split-underlines/split-underlines.pipe";
import { ColorizeDirective }   from "./src/directives/colorize.directive";
import { NavbarComponent }   from "./src/components/navbar/navbar.component";

@NgModule( {
  imports:         [
    CommonModule,FormsModule,
    RouterModule

  ],
  declarations:    [
    SplitUnderlinesPipe,
    ColorizeDirective,
    NavbarComponent
  ],
  providers:[],
  entryComponents: [  ],
  schemas:         [  ],
  exports:[NavbarComponent],
} )

export class MbizSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MbizSharedModule,
      providers: []
    };
  }
}

export * from "./src/pipes/split-underlines/split-underlines.pipe";
export * from "./src/directives/colorize.directive";
export * from "./src/components/navbar/navbar.component";
