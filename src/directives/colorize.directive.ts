import { Directive, ElementRef, Input, OnInit } from "@angular/core";
@Directive({
    selector:'[colorize]'
})
export class ColorizeDirective implements OnInit{

    ngOnInit(): void {
        this.create();
    }

    @Input() colorize:any;

    constructor(private el: ElementRef) {

    }

    create(){
        let nativeEl = this.el.nativeElement;

        let text = this.colorize;
        let textParts = text.split(' ');

        textParts.forEach( ( part:any, index:number ) => {

            let color = `hsl(${(360 * index / text.length)},80%,50%)`;

            let span =  this.createSpan( {
                style: { color: color },
                innerHTML: part
            } );
            nativeEl.appendChild( span );

        } );

    }

    createSpan(config:any){

        let span = document.createElement( 'span' );
            span.style.color = config.style.color;
            span.style.margin = '2px';

            span.innerHTML = config.innerHTML;

        return span;

    }

}