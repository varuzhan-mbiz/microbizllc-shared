import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mbiz-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() childRoutes;

}
