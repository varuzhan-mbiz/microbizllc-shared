import { Pipe, PipeTransform } from '@angular/core';

@Pipe( {
    name: 'splitUnderlines'
} )
export class SplitUnderlinesPipe implements PipeTransform {

    transform( value: string ): string {

        if ( !value ) return null;

        let parts = value.split( '_' ).map( ( part: string ) =>
                        part.charAt( 0 ).toUpperCase() +
                        part.substr( 1 ).toLowerCase()
                    );

        return parts.join( ' ' );

    }

}